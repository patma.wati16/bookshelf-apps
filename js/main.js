loadDataBook();

let addbookbtn = document.getElementById("addbookbtn");
let txtJudul = document.getElementById("inputBookTitle");
let txtAuthor = document.getElementById("inputBookAuthor");
const txtTahun  = document.getElementById("inputBookYear");
const txtisCompleted = document.getElementById("inputBookIsComplete");

addbookbtn.addEventListener("click", function(){
    dtJudul = txtJudul.value;
    dtAuthor = txtAuthor.value;
    dtTahun = txtTahun.value;
    isCompleted = txtisCompleted.checked;
    let id = +new Date();
    if(dtJudul.trim()!=0){
        let webBook = localStorage.getItem("localbook");
        if(webBook == null){
            bookObj = [];
        }
        else{
            bookObj = JSON.parse(webBook);
        }
        bookObj.push({'idbook':id,'judul' : dtJudul, 'author': dtAuthor, 'tahun': dtTahun,'completeStatus': isCompleted});
        localStorage.setItem("localbook", JSON.stringify(bookObj));

    }
    txtJudul.value = "";
    txtAuthor.value = "";
    txtTahun.value = "";
    txtisCompleted.checked= false;
    loadDataBook();
})

function loadDataBook(){
    let webBook = localStorage.getItem("localbook");
    if(webBook == null){
        bookObj = [];
    }
    else{
        bookObj = JSON.parse(webBook);
    }
    let html = '';
    let html2='';
    const uncompletedBooklist = document.getElementById("incompleteBookshelfList");
    const completedBooklist = document.getElementById("completeBookshelfList");
 
    bookObj.forEach((item, index) => {

        if(item.completeStatus==true){
            html += `<tr>
            <th scope="row">${index+1}</th>
            <td>${item.judul}</td>
            <td>${item.author}</td>
            <td>${item.idbook}</td>
            <td>${item.tahun}</td>
            <td><button type="button" onclick="editbook(${index})" class="text-primary"><i class="fa fa-edit"></i>Edit</button></td>
            <td><button type="button" class="text-success" onclick="UncompleteBook(${index})"><i class="fa fa-repeat"></i>UnComplete</button></td>
            <td><button type="button" onclick="deleteitem(${index})" class="text-danger"><i class="fa fa-trash"></i>Delete</button></td>
        </tr>`;
            
        }else{
            html2 += `<tr>
            <th scope="row">${index+1}</th>
            <td>${item.judul}</td>
            <td>${item.author}</td>
            <td>${item.idbook}</td>
            <td>${item.tahun}</td>
            <td><button type="button" onclick="editbook(${index})" class="text-primary"><i class="fa fa-edit"></i>Edit</button></td>
            <td><button type="button" class="text-success" onclick="CompleteBook(${index})"><i class="fa fa-check-square-o"></i>Completed</button></td>
            <td><button type="button" onclick="deleteitem(${index})" class="text-danger"><i class="fa fa-trash"></i>Delete</button></td>
        </tr>`;
        }
        
    });
    completedBooklist.innerHTML = html;
    uncompletedBooklist.innerHTML =html2;
}

function editbook(index){
    let saveindex = document.getElementById("saveindex");
    let addbookbtn = document.getElementById("addbookbtn");
    let savebookbtn = document.getElementById("savebookbtn");
    saveindex.value = index;
    let webBook = localStorage.getItem("localbook");
    let bookObj = JSON.parse(webBook); 


    txtJudul.value = bookObj[index]['judul'];;
    txtAuthor.value = bookObj[index]['author'];
    txtTahun.value = bookObj[index]['tahun'];
    txtisCompleted.checked = bookObj[index]['completeStatus'];
   
    addbookbtn.style.display="none";
    savebookbtn.style.display="block";
}


let savebookbtn = document.getElementById("savebookbtn");
savebookbtn.addEventListener("click", function(){
    let addbookbtn = document.getElementById("addbookbtn");
    let webBook = localStorage.getItem("localbook");
    let bookObj = JSON.parse(webBook); 
    let saveindex = document.getElementById("saveindex").value;
    
    for (keys in bookObj[saveindex]) {
        if(keys == 'idbook'){
            bookObj[saveindex].judul = txtJudul.value;
            bookObj[saveindex].author = txtAuthor.value;
            bookObj[saveindex].tahun = txtTahun.value;
            bookObj[saveindex].completeStatus = txtisCompleted.checked;
            
        }
      }
    savebookbtn.style.display="none";
    addbookbtn.style.display="block";
    localStorage.setItem("localbook", JSON.stringify(bookObj));

    txtJudul.value = "";
    txtAuthor.value = "";
    txtTahun.value = "";
    txtisCompleted.checked= false;
    loadDataBook();
})


function CompleteBook(index){
    let saveindex = index;
    let webBook = localStorage.getItem("localbook");
    let bookObj = JSON.parse(webBook);
    for (keys in bookObj[saveindex]) {
        if(keys == 'idbook'){
            bookObj[saveindex].completeStatus = true;
            
        }
      }    
      localStorage.setItem("localbook", JSON.stringify(bookObj));
       loadDataBook();
    
}

function UncompleteBook(index){
    let saveindex = index;
    let webBook = localStorage.getItem("localbook");
    let bookObj = JSON.parse(webBook);
    for (keys in bookObj[saveindex]) {
        if(keys == 'idbook'){
            bookObj[saveindex].completeStatus = false;
            
        }
      }    
      localStorage.setItem("localbook", JSON.stringify(bookObj));
       loadDataBook();
    
}


function deleteitem(index){
    let webBook = localStorage.getItem("localbook");
    let bookObj = JSON.parse(webBook);
    bookObj.splice(index, 1);
    localStorage.setItem("localbook", JSON.stringify(bookObj));
    loadDataBook();
}

let deleteallbtn = document.getElementById("deleteallbtn");
deleteallbtn.addEventListener("click", function(){
    let savebookbtn = document.getElementById("savebookbtn");
    let addbookbtn = document.getElementById("addbookbtn");
    let webBook = localStorage.getItem("localbook");
    let bookObj = JSON.parse(webBook);
    if(webBook == null){
        bookObj = [];
    }
    else{
        bookObj = JSON.parse(webBook);
        bookObj = [];
    }
    savebookbtn.style.display="none";
    addbookbtn.style.display="block";
    localStorage.setItem("localbook", JSON.stringify(bookObj));
    loadDataBook();

})


let searchtextbox = document.getElementById("searchtextbox");
searchtextbox.addEventListener("input", function(){
    let trlist = document.querySelectorAll("tr");
    Array.from(trlist).forEach(function(item){
        let searchedtext = item.getElementsByTagName("td")[0].innerText;
        let searchtextboxval = searchtextbox.value;
        let re = new RegExp(searchtextboxval, 'gi');
        if(searchedtext.match(re)){
            item.style.display="table-row";
        }
        else{
            item.style.display="none";
        }
    })
})
